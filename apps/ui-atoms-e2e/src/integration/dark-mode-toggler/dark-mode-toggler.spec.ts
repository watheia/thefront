describe('ui-atoms: DarkModeToggler component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=darkmodetoggler--primary'));

  it('should render the component', () => {
    cy.get('h1').should('contain', 'Welcome to DarkModeToggler!');
  });
});

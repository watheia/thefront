describe('ui-atoms: ScrollTop component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=scrolltop--primary'));

  it('should render the component', () => {
    cy.get('h1').should('contain', 'Welcome to ScrollTop!');
  });
});

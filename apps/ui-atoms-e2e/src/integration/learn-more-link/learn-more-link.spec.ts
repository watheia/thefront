describe('ui-atoms: LearnMoreLink component', () => {
  beforeEach(() => cy.visit('/iframe.html?id=learnmorelink--primary'));

  it('should render the component', () => {
    cy.get('h1').should('contain', 'Welcome to LearnMoreLink!');
  });
});

/**
 * Caution: Consider this file when using NextJS or GatsbyJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using react-scripts
 */
import React from 'react';
import NotFoundCover from '@thefront/ui.views.not-found-cover';
import Minimal from '@thefront/layout.minimal';
import WithLayout from '@thefront/layout.with-layout';

const FourOFourPage = (): JSX.Element => {
  return <WithLayout component={NotFoundCover} layout={Minimal} />;
};

export default FourOFourPage;

import Minimal from '@thefront/layout.minimal';
import WithLayout from '@thefront/layout.with-layout';
import ServerError from '@thefront/ui.views.server-error';
import { NextPage } from 'next';
import React from 'react';

const ErrorPage: NextPage = (): JSX.Element => {
  return <WithLayout component={ServerError} layout={Minimal} />;
};

export default ErrorPage;

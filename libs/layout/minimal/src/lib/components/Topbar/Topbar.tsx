import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@mui/styles';
import { Toolbar, Theme } from '@mui/material';
import Image from 'next/image';
import { DarkModeToggler } from '@thefront/ui.atoms';

const useStyles = makeStyles<Theme>((theme) => ({
  toolbar: {
    maxWidth: theme.layout.contentWidth,
    width: '100%',
    margin: '0 auto',
    padding: theme.spacing(0, 2),
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(0, 8),
    },
  },
  logoContainer: {
    minWidth: 170,
    minHeight: 60,
  },
  logoImage: {
    width: '100%',
    height: '100%',
  },
  flexGrow: {
    flexGrow: 1,
  },
}));

interface Props {
  themeMode: string;
  className?: string;
  themeToggler: () => void;
}

const Topbar = ({
  themeMode,
  className,
  themeToggler,
  ...rest
}: Props): JSX.Element => {
  const classes = useStyles();

  return (
    <Toolbar className={clsx(classes['toolbar'], className)} {...rest}>
      <div className={classes['logoContainer']}>
        <a href="/" title="thefront">
          <Image
            className={classes['logoImage']}
            src={
              themeMode === 'light'
                ? '/images/logo.png'
                : '/images/logo-white.png'
            }
            alt="thefront"
            width={170}
            height={60}
          />
        </a>
      </div>
      <div className={classes['flexGrow']} />
      <DarkModeToggler themeMode={themeMode} onClick={themeToggler} />
    </Toolbar>
  );
};

export default Topbar;

import { Divider, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import clsx from 'clsx';
import React, { ReactNode } from 'react';
import { Topbar } from './components';

const useStyles = makeStyles<Theme>((theme) => ({
  root: {
    padding: theme.spacing(0),
  },
  content: {
    height: '100%',
  },
}));

interface Props {
  children: ReactNode;
  themeMode: string;
  themeToggler: () => void;
  className?: string;
}

const Minimal = ({
  themeMode,
  themeToggler,
  children,
  className,
}: Props): JSX.Element => {
  const classes = useStyles();

  return (
    <div className={clsx(classes['root'], className)}>
      <Topbar themeMode={themeMode} themeToggler={themeToggler} />
      <Divider />
      <main className={classes['content']}>{children}</main>
    </div>
  );
};

export default Minimal;

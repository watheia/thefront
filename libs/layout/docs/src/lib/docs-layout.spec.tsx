import { render } from '@testing-library/react';
import { ThemeProvider } from '@thefront/theme';

import DocsLayout from './docs-layout';

describe('thefront.layout/docs', () => {
  it('should render successfully', () => {
    const { container } = render(
      <ThemeProvider>
        <DocsLayout
          children={undefined}
          themeToggler={() => void 0}
          themeMode={'light'}
        />
      </ThemeProvider>
    );
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

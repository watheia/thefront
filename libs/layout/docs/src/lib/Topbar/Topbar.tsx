import React from 'react';
import clsx from 'clsx';
import {
  AppBar,
  Box,
  Hidden,
  IconButton,
  Toolbar,
  List,
  ListItem,
  Button,
  Theme,
} from '@mui/material';
import { Image, DarkModeToggler } from '@thefront/ui.atoms';
import MenuIcon from '@mui/icons-material/Menu';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles<Theme>((theme) => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  logoContainer: {
    width: 100,
    height: 28,
    [theme.breakpoints.up('md')]: {
      width: 120,
      height: 32,
    },
  },
  logoImage: {
    width: '100%',
    height: '100%',
  },
  navigationContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  listItem: {
    paddingRight: 0,
  },
  listItemText: {
    flex: '0 0 auto',
    whiteSpace: 'nowrap',
  },
  listItemButton: {
    whiteSpace: 'nowrap',
  },
  iconButton: {
    paddingRight: 0,
    '&:hover': {
      background: 'transparent',
    },
  },
}));

interface Props {
  className?: string;
  onMobileNavOpen: () => void;
  themeToggler: () => void;
  themeMode: string;
}

const TopBar = ({
  className,
  onMobileNavOpen,
  themeToggler,
  themeMode,
  ...rest
}: Props): JSX.Element => {
  const classes = useStyles();
  return (
    <AppBar
      className={clsx(classes['root'], className)}
      elevation={0}
      color="inherit"
      {...rest}
    >
      <Toolbar>
        <div className={classes['logoContainer']}>
          <a href="/" title="thefront">
            <Image
              className={classes['logoImage']}
              src={
                themeMode === 'light'
                  ? 'https://assets.maccarianagency.com/assets/logos/logo.svg'
                  : 'https://assets.maccarianagency.com/assets/logos/logo-negative.svg'
              }
              alt="thefront"
              lazy={false}
            />
          </a>
        </div>
        <Box flexGrow={1} />
        <DarkModeToggler themeMode={themeMode} onClick={() => themeToggler()} />
        <Hidden smDown>
          <List disablePadding className={classes['navigationContainer']}>
            <ListItem
              className={clsx(classes['listItem'], 'menu-item--no-dropdown')}
            >
              <Button
                className={classes['listItemText']}
                component="a"
                href="/"
                variant="outlined"
              >
                SEE ALL PAGES
              </Button>
            </ListItem>
            <ListItem
              className={clsx(classes['listItem'], 'menu-item--no-dropdown')}
            >
              <Button
                variant="contained"
                color="primary"
                component="a"
                target="blank"
                href="https://material-ui.com/store/items/the-front-landing-page/"
                className={classes['listItemButton']}
              >
                Buy Now
              </Button>
            </ListItem>
          </List>
        </Hidden>
        <Hidden mdUp>
          <IconButton
            onClick={() => onMobileNavOpen()}
            className={classes['iconButton']}
            aria-label="Menu"
            disableRipple
          >
            <MenuIcon />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
};

export default TopBar;

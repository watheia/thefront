import { Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ScrollTop } from '@thefront/ui.atoms';
import React, { useState } from 'react';
import { Navbar, Topbar } from '.';

const useStyles = makeStyles<Theme>((theme) => ({
  root: {
    display: 'flex',
    height: '100%',
    overflow: 'hidden',
    width: '100%',
  },
  wrapper: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
    paddingTop: 64,
    [theme.breakpoints.up('md')]: {
      paddingLeft: 256,
    },
  },
  contentContainer: {
    display: 'flex',
    flex: '1 1 auto',
    overflow: 'hidden',
  },
  content: {
    flex: '1 1 auto',
    height: '100%',
    overflow: 'auto',
  },
}));

interface Props {
  children: React.ReactNode;
  themeToggler: () => void;
  themeMode: string;
}

const DocsLayout = ({
  children,
  themeToggler,
  themeMode,
}: Props): JSX.Element => {
  const classes = useStyles();
  const [isMobileNavOpen, setMobileNavOpen] = useState(false);

  return (
    <div className={classes['root']}>
      <Topbar
        themeMode={themeMode}
        themeToggler={themeToggler}
        onMobileNavOpen={() => setMobileNavOpen(true)}
      />
      <Navbar
        onMobileClose={() => setMobileNavOpen(false)}
        openMobile={isMobileNavOpen}
      />
      <div className={classes['wrapper']}>
        <div className={classes['contentContainer']}>
          <div className={classes['content']}>{children}</div>
        </div>
      </div>
      <ScrollTop />
    </div>
  );
};

export default DocsLayout;

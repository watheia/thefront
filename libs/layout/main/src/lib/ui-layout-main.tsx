import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiLayoutMainProps {}

const StyledUiLayoutMain = styled.div`
  color: pink;
`;

export function UiLayoutMain(props: UiLayoutMainProps) {
  return (
    <StyledUiLayoutMain>
      <h1>Welcome to UiLayoutMain!</h1>
    </StyledUiLayoutMain>
  );
}

export default UiLayoutMain;

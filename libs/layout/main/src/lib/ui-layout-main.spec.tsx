import { render } from '@testing-library/react';

import UiLayoutMain from './ui-layout-main';

describe('UiLayoutMain', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiLayoutMain />);
    expect(baseElement).toBeTruthy();
  });
});

import { PaletteMode } from '@mui/material';
import { useState, useCallback, useEffect } from 'react';
import aos from 'aos';

const STORAGE_ID = 'thefront.theme/use-dark-mode';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface UseDarkMode {
  mode: PaletteMode;
  toggle: () => void;
  isMounted: boolean;
}

export function useDarkMode(): UseDarkMode {
  const [mode, setPaletteMode] = useState<PaletteMode>('light');
  const [isMounted, setMounted] = useState(false);

  const toggle = useCallback(
    () => setPaletteMode((x) => (x === 'light' ? 'dark' : 'light')),
    []
  );

  // Run once on init
  useEffect(() => {
    // const localTheme = window.localStorage.getItem('themeMode');
    // localTheme ? setTheme(localTheme) : setMode('light');
    console.log('Mounted Layout');
    setMounted(true);
    aos.refresh();
  }, []);

  // run after theme mode changes
  useEffect(() => {
    console.log('Theme mode changed to', mode);
    window.localStorage.setItem(STORAGE_ID, mode);
    aos.refresh();
  }, [mode]);

  return { mode, toggle, isMounted };
}

export default useDarkMode;

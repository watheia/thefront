import { ThemeProvider } from '@thefront/theme';
import aos from 'aos';
import React, { useEffect } from 'react';
import { useDarkMode } from './use-dark-mode';

interface Props {
  layout: any;
  component: any;
  // All other props
  [x: string]: any;
}

export default function TopLayout({
  component: Component,
  layout: Layout,
  ...rest
}: Props): JSX.Element {
  const { mode, toggle: toggleThemeMode, isMounted } = useDarkMode();

  useEffect(() => {
    // Remove the server-side injected CSS.
    // TODO find out if this is still needed for MUI5
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles && jssStyles.parentElement) {
      jssStyles.parentElement.removeChild(jssStyles);
    }

    aos.init({
      once: true,
      delay: 50,
      duration: 500,
      easing: 'ease-in-out',
    });
  }, []);

  if (!isMounted) return <div />;

  return (
    <ThemeProvider mode={mode}>
      <Layout themeMode={mode} themeToggler={toggleThemeMode}>
        <Component themeMode={mode} {...rest} />
      </Layout>
    </ThemeProvider>
  );
}

import { act, renderHook } from '@testing-library/react-hooks';
import useDarkMode from './use-dark-mode';

describe('useDarkMode', () => {
  it('should render successfully', () => {
    const { result } = renderHook(() => useDarkMode());

    expect(result.current.isMounted).toBe(true);
    expect(result.current.mode).toBe('light');
    expect(result.current.toggle).toBeInstanceOf(Function);

    act(() => {
      result.current.toggle();
    });

    expect(result.current.mode).toBe('dark');
  });
});

// export type { WaTheme, WaThemeOptions } from './lib/theme';
export { default as createTheme } from './lib/theme';
export { default as createEmotionCache } from './lib/create-emotion-cache';
export { default as ThemeProvider } from './lib/theme-provider';
export { light, dark } from './lib/palette';

import {
  createTheme as createMuiTheme,
  responsiveFontSizes,
  PaletteMode,
} from '@mui/material';
import { light, dark } from './palette';

declare module '@mui/material/styles' {
  interface Theme {
    layout: {
      contentWidth: number | string;
    };
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    layout?: {
      contentWidth: number | string;
    };
  }
}

const createTheme = (mode: PaletteMode) =>
  responsiveFontSizes(
    createMuiTheme({
      palette: mode === 'light' ? light : dark,
      layout: {
        contentWidth: 1236,
      },
      typography: {
        fontFamily: 'Roboto',
      },
      zIndex: {
        appBar: 1200,
        drawer: 1100,
      },
    })
  );

export default createTheme;

import {
  CssBaseline,
  PaletteMode,
  Paper,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material';
import { ReactNode, useMemo } from 'react';
import createTheme from './theme';

export interface ThemeProviderProps {
  mode?: PaletteMode;
  children?: ReactNode | ReactNode[] | null;
}

export default function ThemeProvider({
  children,
  mode = 'light',
}: ThemeProviderProps): JSX.Element {
  const theme = useMemo(() => createTheme(mode), [mode]);
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      <Paper sx={{ height: '100vh' }} elevation={0}>
        {children}
      </Paper>
    </MuiThemeProvider>
  );
}

ThemeProvider.__id = 'thefront.theme/theme-provider';

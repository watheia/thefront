import { Button } from '@mui/material';
import { render } from '@testing-library/react';

import ThemeProvider from './theme-provider';

describe(ThemeProvider.__id, () => {
  it('should render successfully', () => {
    const { container } = render(
      <ThemeProvider>
        <Button>Styled Button</Button>
      </ThemeProvider>
    );
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

import { render } from '@testing-library/react';

import UiViewsNotFound from './ui-views-not-found';

describe('UiViewsNotFound', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiViewsNotFound />);
    expect(baseElement).toBeTruthy();
  });
});

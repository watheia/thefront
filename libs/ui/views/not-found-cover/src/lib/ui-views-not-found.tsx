import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiViewsNotFoundProps {}

const StyledUiViewsNotFound = styled.div`
  color: pink;
`;

export function UiViewsNotFound(props: UiViewsNotFoundProps) {
  return (
    <StyledUiViewsNotFound>
      <h1>Welcome to UiViewsNotFound!</h1>
    </StyledUiViewsNotFound>
  );
}

export default UiViewsNotFound;

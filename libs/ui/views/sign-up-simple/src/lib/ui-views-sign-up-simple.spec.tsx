import { render } from '@testing-library/react';

import UiViewsSignUpSimple from './ui-views-sign-up-simple';

describe('UiViewsSignUpSimple', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiViewsSignUpSimple />);
    expect(baseElement).toBeTruthy();
  });
});

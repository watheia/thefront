import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiViewsSignUpSimpleProps {}

const StyledUiViewsSignUpSimple = styled.div`
  color: pink;
`;

export function UiViewsSignUpSimple(props: UiViewsSignUpSimpleProps) {
  return (
    <StyledUiViewsSignUpSimple>
      <h1>Welcome to UiViewsSignUpSimple!</h1>
    </StyledUiViewsSignUpSimple>
  );
}

export default UiViewsSignUpSimple;

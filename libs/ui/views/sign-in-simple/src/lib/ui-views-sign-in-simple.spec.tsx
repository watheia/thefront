import { render } from '@testing-library/react';

import UiViewsSignInSimple from './ui-views-sign-in-simple';

describe('UiViewsSignInSimple', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiViewsSignInSimple />);
    expect(baseElement).toBeTruthy();
  });
});

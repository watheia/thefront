import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiViewsSignInSimpleProps {}

const StyledUiViewsSignInSimple = styled.div`
  color: pink;
`;

export function UiViewsSignInSimple(props: UiViewsSignInSimpleProps) {
  return (
    <StyledUiViewsSignInSimple>
      <h1>Welcome to UiViewsSignInSimple!</h1>
    </StyledUiViewsSignInSimple>
  );
}

export default UiViewsSignInSimple;

import { render } from '@testing-library/react';

import UiViewsServerError from './ui-views-server-error';

describe('UiViewsServerError', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiViewsServerError />);
    expect(baseElement).toBeTruthy();
  });
});

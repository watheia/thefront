import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiViewsServerErrorProps {}

const StyledUiViewsServerError = styled.div`
  color: pink;
`;

export function UiViewsServerError(props: UiViewsServerErrorProps) {
  return (
    <StyledUiViewsServerError>
      <h1>Welcome to UiViewsServerError!</h1>
    </StyledUiViewsServerError>
  );
}

export default UiViewsServerError;

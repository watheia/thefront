import { render } from '@testing-library/react';

import UiViewsDocumentation from './ui-views-documentation';

describe('UiViewsDocumentation', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UiViewsDocumentation />);
    expect(baseElement).toBeTruthy();
  });
});

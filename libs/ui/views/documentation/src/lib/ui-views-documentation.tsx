import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface UiViewsDocumentationProps {}

const StyledUiViewsDocumentation = styled.div`
  color: pink;
`;

export function UiViewsDocumentation(props: UiViewsDocumentationProps) {
  return (
    <StyledUiViewsDocumentation>
      <h1>Welcome to UiViewsDocumentation!</h1>
    </StyledUiViewsDocumentation>
  );
}

export default UiViewsDocumentation;

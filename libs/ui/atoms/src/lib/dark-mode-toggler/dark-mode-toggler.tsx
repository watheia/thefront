import * as React from 'react';
import DarkMode from '@mui/icons-material/DarkMode';
import LightMode from '@mui/icons-material/LightMode';
import ToggleButton from '@mui/material/ToggleButton';
import { HtmlHTMLAttributes, useState } from 'react';

export interface DarkModeTogglerProps extends HtmlHTMLAttributes<HTMLElement> {
  /**
   * The theme mode
   */
  themeMode?: string;
  /**
   * Theme toggler function
   */
  onClick: (event: React.MouseEvent) => void;
  /**
   * Color of the icon
   */
  fontIconColor?: string;
}

/**
 * Component to display the dark mode toggler
 *
 * @param {Object} props
 */
const DarkModeToggler = ({
  themeMode = 'light',
  onClick,
  className,
  ...rest
}: DarkModeTogglerProps): JSX.Element => {
  const [selected, setSelected] = useState(themeMode === 'dark');
  return (
    <ToggleButton
      value="check"
      selected={selected}
      size="small"
      onClick={onClick}
      onChange={() => {
        setSelected(!selected);
      }}
    >
      {selected ? <LightMode /> : <DarkMode />}
    </ToggleButton>
  );
};

export default DarkModeToggler;

DarkModeToggler.__id = 'thefront.ui/atoms/dark-mode-toggler';

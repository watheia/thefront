import { MouseEvent, useState } from 'react';
import { Story, Meta } from '@storybook/react';
import DarkModeToggler from './dark-mode-toggler';
import { PaletteMode } from '@mui/material';

export default {
  component: DarkModeToggler,
  title: 'DarkModeToggler',
} as Meta;

const Template: Story = (args) => {
  const [mode, setMode] = useState<PaletteMode>('light');
  const toggle = (): void => {
    setMode(mode === 'dark' ? 'light' : 'dark');
  };
  return <DarkModeToggler themeMode={mode} onClick={toggle} {...args} />;
};

export const Primary = Template.bind({});
Primary.args = {};

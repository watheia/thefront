import { render } from '@testing-library/react';
import { ThemeProvider } from '@thefront/theme';
import DarkModeToggler from './dark-mode-toggler';

describe(DarkModeToggler.__id, () => {
  it('should render successfully', () => {
    const { container } = render(
      <ThemeProvider>
        <DarkModeToggler onClick={() => void 0} />
      </ThemeProvider>
    );
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

import { Story, Meta } from '@storybook/react';
import IconText from './icon-text';

export default {
  component: IconText,
  title: 'IconText',
} as Meta;

const Template: Story = (args) => (
  <IconText fontIconClass="plus" title="Icon Title" {...args} />
);

export const Primary = Template.bind({});
Primary.args = {};

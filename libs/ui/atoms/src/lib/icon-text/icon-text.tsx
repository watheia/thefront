import React, { HtmlHTMLAttributes } from 'react';
import clsx from 'clsx';
import { Typography, styled } from '@mui/material';
import Icon from '../icon';

const Root = styled('div')({
  display: 'inline-flex',
  flexWrap: 'nowrap',
  alignItems: 'center',
  width: '100%',
});

const Title = styled('div')({
  marginLeft: 1,
});

export interface IconTextProps extends HtmlHTMLAttributes<HTMLDivElement> {
  /**
   * The classes of the font icon
   */
  fontIconClass: string;

  /**
   * Source set for the responsive images
   */
  color?: string;

  /**
   * Title of the icon-text
   */
  title: string;

  /**
   * Additional properties to pass to the Icon component
   */
  iconProps?: Record<string, any>;

  /**
   * Additional properties to pass to the Typography component
   */
  typographyProps?: Record<string, any>;
}

/**
 * Component to display the icon text
 *
 * @param {Object} props
 */
const IconText = ({
  title,
  color,
  fontIconClass,
  className,
  iconProps = {},
  typographyProps = {},
  ...rest
}: IconTextProps): JSX.Element => {
  return (
    <Root className={clsx('icon-text', className)} {...rest}>
      <Icon
        className="icon-text__icon"
        size="small"
        fontIconClass={fontIconClass}
        fontIconColor={color}
        {...iconProps}
      />
      <Typography
        noWrap
        component={Title}
        variant="subtitle1"
        color="textPrimary"
        className="icon-text__title"
        {...typographyProps}
      >
        {title}
      </Typography>
    </Root>
  );
};

export default IconText;

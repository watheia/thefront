import { render } from '@testing-library/react';

import IconText from './icon-text';

describe('IconText', () => {
  it('should render successfully', () => {
    const { container } = render(
      <IconText title="TEXT" fontIconClass="plus" />
    );
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

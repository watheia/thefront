import { render } from '@testing-library/react';

import LearnMoreLink from './learn-more-link';

describe('LearnMoreLink', () => {
  it('should render successfully', () => {
    const { container } = render(<LearnMoreLink />);
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

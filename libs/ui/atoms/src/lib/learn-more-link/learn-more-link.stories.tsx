import { Story, Meta } from '@storybook/react';
import LearnMoreLink from './learn-more-link';

export default {
  component: LearnMoreLink,
  title: 'LearnMoreLink',
} as Meta;

const Template: Story = (args) => <LearnMoreLink {...args} />;

export const Primary = Template.bind({});
Primary.args = {};

import React, { HtmlHTMLAttributes } from 'react';
import clsx from 'clsx';
import { styled, Typography, IconButton, useTheme } from '@mui/material';
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

const Anchor = styled('a')({
  display: 'inline-flex',
  alignItems: 'center',
  textDecoration: 'none',
});

export interface LearnMoreLinkProps
  extends HtmlHTMLAttributes<HTMLAnchorElement> {
  /**
   * The component to load as a main DOM
   */
  component?: 'Link' | 'a';
  /**
   * Title of the link
   */
  title?: string;
  /**
   * Variant of the link
   */
  variant?: 'h6' | 'subtitle1' | 'subtitle2' | 'body1' | 'body2';
  /**
   * Href of the link
   */
  href?: string;
  /**
   * Color of the link
   */
  color?:
    | 'inherit'
    | 'initial'
    | 'primary'
    | 'secondary'
    | 'textPrimary'
    | 'textSecondary'
    | 'error'
    | undefined;
  /**
   * Additional properties to pass to the Icon component
   */
  iconProps?: Record<string, any>;
  /**
   * Additional properties to pass to the Typography component
   */
  typographyProps?: Record<string, any>;
}

/**
 * Component to display the "Learn More" link
 *
 * @param {Object} props
 */
const LearnMoreLink = ({
  color,
  component = 'a',
  variant = 'subtitle1',
  title = 'learn more',
  href = '#',
  className,
  iconProps = {},
  typographyProps = {},
  ...rest
}: LearnMoreLinkProps): JSX.Element => {
  const theme = useTheme();

  return (
    <Anchor
      href={href}
      className={clsx('learn-more-link', className)}
      {...rest}
    >
      <Typography
        component="span"
        className={clsx('learn-more-link__typography')}
        fontWeight="bold"
        variant={variant}
        color={color || 'primary'}
        {...typographyProps}
      >
        {title}
      </Typography>
      <IconButton
        className={clsx('learn-more-link__icon-button')}
        sx={{
          padding: 0,
          marginLeft: theme.spacing(1),
          '&:hover': {
            background: 'transparent',
          },
        }}
        color={'primary'}
        {...iconProps}
      >
        <ArrowRightAltIcon className="learn-more-link__arrow" />
      </IconButton>
    </Anchor>
  );
};

export default LearnMoreLink;

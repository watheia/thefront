import { Story, Meta } from '@storybook/react';
import Icon from './icon';

export default {
  component: Icon,
  title: 'Icon',
} as Meta;

const Template: Story = (args) => <Icon fontIconClass="plus" {...args} />;

export const Primary = Template.bind({});
Primary.args = {};

import NoSsr from '@mui/material/NoSsr';
import clsx from 'clsx';
import React from 'react';

const fontSizes = {
  extraSmall: 10,
  small: 20,
  medium: 30,
  large: 40,
};

export interface IconProps {
  /**
   * External classes
   */
  className?: string;
  /**
   * The classes of the font icon
   */
  fontIconClass: string;
  /**
   * Source set for the responsive images
   */
  size?: 'extraSmall' | 'small' | 'medium' | 'large';
  /**
   * Color of the icon
   */
  fontIconColor?: string;
}

/**
 * Component to display the icon
 *
 * @param {Object} props
 */
const Icon = ({
  fontIconClass,
  size = 'small',
  fontIconColor,
  className,
  ...rest
}: IconProps): JSX.Element => {
  return (
    <NoSsr>
      <i
        className={clsx('icon', fontIconClass, className)}
        style={{ color: fontIconColor, fontSize: fontSizes[size] }}
        {...rest}
      />
    </NoSsr>
  );
};

export default Icon;

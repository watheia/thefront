import { Box, Container } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import ScrollTop from './scroll-top';

export default {
  component: ScrollTop,
  title: 'ScrollTop',
} as Meta;

const Template: Story = (args) => {
  return (
    <>
      <Container>
        <Box sx={{ my: 2 }}>
          {[...new Array(12)]
            .map(
              () => `Cras mattis consectetur purus sit amet fermentum.
Cras justo odio, dapibus ac facilisis in, egestas eget quam.
Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
Praesent commodo cursus magna, vel scelerisque nisl consectetur et.`
            )
            .join('\n')}
        </Box>
      </Container>
      <ScrollTop {...args} />
    </>
  );
};
export const Primary = Template.bind({});
Primary.args = {};

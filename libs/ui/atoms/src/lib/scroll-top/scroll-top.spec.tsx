import { render } from '@testing-library/react';
import { ThemeProvider } from '@thefront/theme';

import ScrollTop from './scroll-top';

describe('ScrollTop', () => {
  it('should render successfully', () => {
    const { container } = render(
      <ThemeProvider>
        <ScrollTop onClick={() => void 0} />
      </ThemeProvider>
    );
    expect(container).toBeInstanceOf(HTMLDivElement);
    expect(container).toMatchSnapshot();
  });
});

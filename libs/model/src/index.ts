export type { User, Post } from '@prisma/client';

export { main } from './seed';
export { default } from './prisma';

import { PrismaClient } from '@prisma/client';

// PrismaClient is attached to the `global` object in development to prevent
// exhausting your database connection limit.
//
// Learn more:
// https://pris.ly/d/help/next-js-best-practices

let model: PrismaClient;

if (process.env['NODE_ENV'] === 'production') {
  model = new PrismaClient();
} else {
  if (!(global as any).model) {
    (global as any).model = new PrismaClient();
  }
  model = (global as any).model;
}
export default model;
